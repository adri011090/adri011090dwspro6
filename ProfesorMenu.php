
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php include_once("funciones.php"); ?>
        <?php include_once("modelo.php"); ?>
        <?php cabecera(); ?>

        <p>Gestión de Profesor:</p>
        <ul>
            <li><a href="ProfesorFormulario.html">Alta </a> </li>
        </ul>

        <?php
        echo "<p>Listado:</p>";

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id Prof</td>';
        echo '<td>Nom Prof</td>';
        echo '</tr>';

        $modelo = new Modelo();
        $profesores = $modelo->getProfesores();

        if (count($profesores) > 0) {

            foreach ($profesores as $profesor) {
                echo "<tr>";
                echo "<td>" . $profesor->getId() . "</td>";
                echo "<td>" . $profesor->getNombre() . "</td>";
                echo "</tr>";
            }
        }

        echo "</table>";
        echo "<br/>";
        ?>

        <?php inicio(); ?>
        <?php pie(); ?>

    </body>
</html>