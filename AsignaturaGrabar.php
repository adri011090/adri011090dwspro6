<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        include_once("modelo.php");


        function recoge($campo) {

            if (isset($_REQUEST[$campo])) {
                $valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
            } else {
                $valor = "";
            };
            return $valor;
        }

        function leerAsignatura() {

            $id = recoge("id");
            $nombre = recoge("nombre");
            $horas = recoge("horas");
            $profesorid = recoge("profesor");

            $profesor_ = new Profesor($profesorid, "");
            $modelo = new Modelo();
            $profesor = $modelo->getProfesor($profesor_);
            $asignatura = new Asignatura($id, $nombre, $horas, $profesor);



            return $asignatura;
        }


        $asignatura = leerAsignatura();


        if ($asignatura->getId() == "") {
            echo "Error: Id asignatura vacio" . "<br>";
        } else if ($asignatura->getNombre() == "") {
            echo "Error: Nombre asignatura vacio" . "<br>";
        } else if ($asignatura->getHoras() == "") {
            echo "Error: Horas vacio" . "<br>";
        } else if ($asignatura->getProfesor()->getId() == "") {
            echo "Error: Nombre profesor vacio" . "<br>";
        } else {
            $modelo = new Modelo();
            $modelo->grabarAsignatura($asignatura);
            echo "Grabado: " . $asignatura->getNombre() . "<br>";
        }


        echo "<a href='index.php'>Volver</a>";
        ?>
    </body>
</html>