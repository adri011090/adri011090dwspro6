<?php

require_once "modelo.php";

class ModeloMysql implements modelo {

    public function conectar() {
	$pdo = null;
	try {
	    $pdo = new PDO("mysql:host=localhost;dbname=" . Configuracion::$bdnombre, Configuracion::$bdusuario, Configuracion::$bdclave);
	    $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
	} catch (PDOException $e) {
	    echo "<p>Error: No puede conectarse con la base de datos.</p>\n";
	    echo "<p>Error: " . $e->getMessage() . "</p>\n";
	}
	return $pdo;
    }

    public function desconectar() {
	return null;
    }

    public function instalar() {

	try {
	    $pdo = new PDO("mysql:host=localhost", Configuracion::$bdusuario, Configuracion::$bdclave);
	    $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
	} catch (PDOException $e) {
	    echo "<p>Error: No puede conectarse con la base de datos.</p>\n";
	    echo "<p>Error: " . $e->getMessage() . "</p>\n";
	}

	$consulta = "CREATE DATABASE " . Configuracion::$bdnombre;

	$pdo->query($consulta);

	$pdo = $this->desconectar();

	$pdo = $this->conectar();

	$consulta = "CREATE TABLE profesores ("
		. "id INT AUTO_INCREMENT,"
                . "nombre VARCHAR(15),"
		. "PRIMARY KEY (id));"
		. "CREATE TABLE asignaturas ("
		. "id INT AUTO_INCREMENT,"
		. "nombre VARCHAR(20),"
		. "horas INT,"
		. "idprofesor INT,"
		. "PRIMARY KEY (id),"
		. "FOREIGN KEY (idprofesor) REFERENCES profesores(id));";
	$pdo->query($consulta);

	$consulta = "INSERT INTO profesores (nombre) VALUES ('Adri');"
		. "INSERT INTO profesores (nombre) VALUES ('Juan');"
		. "INSERT INTO asignaturas (nombre, horas, idprofesor) VALUES ('ED', '5', 1);"
		. "INSERT INTO asignaturas (nombre, horas, idprofesor) VALUES ('DWEC', '3', 1);";
	$pdo->query($consulta);

	$pdo = $this->desconectar();
    }

    public function createProfesor($profesor) {
	$pdo = $this->conectar();
	$consulta = "INSERT INTO profesores (nombre) VALUES (:nombre);";
	$pdo->prepare($consulta)->execute(array(":nombre" => $profesor->__GET('nombre')));
	$pdo = $this->desconectar();
    }

    public function readProfesor() {
	$profesores = array();
	$pdo = $this->conectar();
	$consulta = "SELECT * FROM profesores";
	$stm = $pdo->prepare($consulta);
	$stm->execute();
	foreach($stm->fetchAll(PDO::FETCH_OBJ) as $row){
	    $profesor = new Profesor(0, "","");
	    $profesor->__SET('id', $row->id);
            $profesor->__SET('nombre', $row->nombre);
	    
	    $profesores[] = $profesor;
	}
	$pdo = $this->desconectar();
	return $profesores;
    }

    public function createAsignatura($asignatura) {
	$pdo = $this->conectar();
	$consulta = "INSERT INTO asignaturas (nombre, horas, idprofesor) VALUES (:nombre, :horas, :idprofesor);";
	$pdo->prepare($consulta)->execute(array(":nombre"      => $perro->__GET('nombre'), 
						":horas"        => $perro->__GET('horas'), 
						":idprofesor"       => $perro->__GET('idprofesor'), 
                                                ));
	$pdo = $this->desconectar();
    }

    public function readAsignatura() {
	$asignaturas = array();
	$pdo = $this->conectar();
	$consulta = "SELECT * FROM asignaturas";
	$stm = $pdo->prepare($consulta);
	$stm->execute();
	foreach($stm->fetchAll(PDO::FETCH_OBJ) as $row){
	    $asignatura = new Asignatura(0, "","","",null);
	    $profesor = new Profesor(0, "","");
	    
	    $profesor->__SET('id', $row->idprofesor);
	    
	    $asignatura->__SET('id', $row->id);
	    $asignatura->__SET('nombre', $row->nombre);
	    $asignatura->__SET('horas', $row->horas);
	    $asignatura->__SET('idprofesor', $profesor);
	               
	    $asignaturas[] = $asignatura;
	}
	$pdo = $this->desconectar();
	return $asignaturas;
    }

    public function idProfesor() {
	$pdo = $this->conectar();  
	$id = $pdo->query("SELECT MAX(id) AS id FROM profesores")->fetch(PDO::FETCH_NUM);
	$pdo = $this->desconectar();
	return $id[0] + 1;
    }

    public function idAsignatura() {
	$pdo = $this->conectar();  
	$id = $pdo->query("SELECT MAX(id) AS id FROM asignaturas")->fetch(PDO::FETCH_NUM);
	$pdo = $this->desconectar();
	return $id[0] + 1;
    }

}
