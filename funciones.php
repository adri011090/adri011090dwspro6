<?php

include_once("Config.php");
include_once("Profesor.php");
include_once("Asignatura.php");
include_once("ModeloMysql.php");


function cabecera() {
    echo "<h1>" . Config::$titulo . "</h1><hr/>\n";
}

function pie() {
    echo "<hr/><pre>" . Config::$empresa . " " . Config::$autor . " ";
    echo Config::$curso . " " . Config::$fecha . "</pre>\n";
}

function inicio() {
    echo "<align='right'><a href = 'index.php'>Inicio</a> </align>\n";
}

function comprobarModelo() {

    if ($_SESSION['modelo'] === "mysql") {
	$modeloElegido = new ModeloMysql();
    }
    return $modeloElegido;
}
?>