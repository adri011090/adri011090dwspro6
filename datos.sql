CREATE DATABASE base_datos;

USE base_datos;

CREATE TABLE profesores (
		id INT AUTO_INCREMENT,
		nombre VARCHAR(15),
		PRIMARY KEY (id));

CREATE TABLE asignaturas (
		id INT AUTO_INCREMENT,
		nombre VARCHAR(20),
		horas INT,
		idprofesor INT,
		PRIMARY KEY (id),
		FOREIGN KEY (idprofesor) REFERENCES profesores(id));

