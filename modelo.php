<?php

interface modelo {
    
    public function instalar();

    public function createProfesor($profesor);
    public function readProfesor();
    public function idProfesor();
    
    public function createAsignatura($asignatura);
    public function readAsignatura();
    public function idAsignatura();
    
}