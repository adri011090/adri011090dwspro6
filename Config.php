<?php

class Config {

    static public $titulo = "Gestión de Asignaturas";
    static public $autor = "Adrián Pareja Peñalver";
    static public $fecha = "21/01/2017";
    static public $empresa = "CEEDCV";
    static public $curso = "2016-17";
    static public $modelo = "mysql";
    static public $bdnombre = "base_datos";
    static public $bdusuario = "alumno";
    static public $bdclave = "alumno";
    static public $bdhostname = "localhost";
}
?>
